/*   
 * Project: OSMP
 * FileName: ExecutorWrapper.java
 * version: V1.0
 */
package com.osmp.web.demo.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.demo.entity.DemoStudent;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年10月13日 下午3:14:56
 */

public interface DemoStudentMapper extends BaseMapper<DemoStudent> {

}
